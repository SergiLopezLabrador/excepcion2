package exceptions;

public class excepcionNueva extends Exception{
    
	//Aqu� creo la variable tipo String tipo private
    private String MensajeEnCasoError;
     
    //Aqu� creo un constructor donde le otorgo valor a la variable 
    public excepcionNueva(String MensajeEnCasoError){
    	
        this.MensajeEnCasoError=MensajeEnCasoError;
    }
     
    //Aqu� creo un m�todo donde genero un mensaje de error al recibir el parametro en formatro string que 
    //proviene del formato padre
    
    public String MensajeError(){
         
    	//Aqu� creo la variable que utilizar� para crear el mensaje de error
        String Informacion="Prueba";
         
        //Aqu� creo un switch case donde su objetivo �s mostrar el mensaje cuando reciba el parametro
        //"hola" (en mi caso)
        switch(MensajeEnCasoError){
            case "error":
            	Informacion="Excepci�n capturada con mensaje: Esto es un objeto Exception";
                break;
        }
         
        //Finalmente devolvemos el mensaje
        return Informacion;
         
    }
     
}
