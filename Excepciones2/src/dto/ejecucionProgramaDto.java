package dto;

import exceptions.excepcionNueva;

public class ejecucionProgramaDto {
	
	//Aqu� voy a crear un m�todo donde agrupar� todos los m�todos y los encapsular� en un solo m�todo
	//para posteriormente ejecutarlo al Main
	public static void ejecutarPrograma() {
		
		//Aqu� creo la variable tipo String que ser� analizada
        String palabraErronea = "Sergi";
        
        
        try {
        	//Aqu� uso un try para analizar la variable
        	palabraErronea = "Sergi";
 
        	//En el caso de que la palabra sea "Sergi", se enviar� una se�al a la expeci�n de error
            if (palabraErronea.contentEquals("Sergi")) {
                throw new excepcionNueva("error");     
            } 
        } 
        
        //Finalmente, mostramos el mensaje del error por consola
        catch (excepcionNueva ser) {
        	System.out.println("Mensaje mostrado por pantalla: ");
            System.out.println(ser.MensajeError());
        }
        
        //Aqu� muestro que el programa a finalizado
        System.out.println("Programa terminado");
		

    }

}
